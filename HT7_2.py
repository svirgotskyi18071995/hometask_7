# Помістіть в lib.py декоратор для вимірювання часу.
# Імпортуйте декоратор в основний файл, задекоруйте основну функцію "Касир".
import Lib_with_decorator
from Lib_with_decorator import time_decorator
tsk1 = '---------------------------------------- Task 3 ------------------------------------------------'
space = '------------------------------------------------------------------------------------------------'
print(space)
print(tsk1)
print(space)
@time_decorator
def final_func():
    while True:
        input_age = input('Enter please your age.\n')
        result_checker = Lib_with_decorator.checker(input_age)
        if result_checker == 0:
            print('Error input! Try again!\n')
            continue
        else:
            age = result_checker
            break
            print(space)
    final_result = Lib_with_decorator.compare(age)
    print(space)

final_func()
