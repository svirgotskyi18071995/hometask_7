


def checker(value):
    value.strip()
    if value.isdigit():
        int_value = int(value)
        if int_value >= 0:
            return int_value
    else:
        return 0

def print_options(option):
    str_option = str(option)
    if str_option.endswith('1'):
        if 5 <= option <= 20:
            return 'років'
        else:
            return 'рік'
    elif str_option.endswith('2') or str_option.endswith('3') or str_option.endswith('4'):
        if 5 <= option <= 20:
            return 'років'
        else:
            return 'роки'
    else:
        return 'років'
def compare(user_age):
    str_user_age = str(user_age)
    aditional_option = print_options(user_age)
    if user_age < 7:
        return print(f'Тобі ж {user_age} {aditional_option}, ! Де твої батьки?')
    elif 7 <= user_age < 16:
        if '7' in str_user_age:
            return print(f'Тобі лише {user_age} {aditional_option}, а це фільм для дорослих! А також  вам пощастить')
        else:
            return print(f'Тобі лише {user_age} {aditional_option}, а це фільм для дорослих!')

    elif user_age > 65:

        if '7' in str_user_age:
            return print(f'Вам {user_age} {aditional_option}? Покажіть пенсійне посвідчення! А також  вам пощастить')
        else:
            return print(f'Вам {user_age} {aditional_option}? Покажіть пенсійне посвідчення!')
    elif '7' in str_user_age:
        return print(f'Вам {user_age} {aditional_option}, вам пощастить')
    else:
        return print(f'Незважаючи на те, що вам {user_age} {aditional_option}, білетів всеодно нема!')

